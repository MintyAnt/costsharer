var gulp = require('gulp');
var babelify = require('babelify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var browserSync = require('browser-sync').create();

gulp.task('default', ['bundle', 'serve', 'watch']);

gulp.task('bundle', function() {
    return browserify({
            entries: './src/js/index.js',
            debug: true
        })
        .transform(babelify)
        .bundle()
        .on('error', function (err) { console.log('Error : ' + err.message); })
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('public/js'))
});

gulp.task('serve', function() {
    browserSync.init({
        port: 3000,
        server: {
            baseDir: './public/'
        }
    });
});

gulp.task('watch', function() {
    gulp.watch('./src/js/**', ['bundle']);
    gulp.watch('./public/**', browserSync.reload);
});