
import _ from 'lodash';
import React from 'react';
import { connect } from 'react-redux';

import * as actionCreators from './redux/action-creators';
import Person from './person.js';
import { calculateTaxPercent, calculateTipPercent } from './utils.js';

const PeopleContainer = React.createClass({
    propTypes: {
        people: React.PropTypes.array.isRequired,
        items: React.PropTypes.array.isRequired,
    },
    getItemsBoughtForPerson(person, items) {
        return _.filter(items, function(currentItem) {
            return currentItem.purchaser.id === person.id;
        });
    },
    addPerson(event) {
        event.preventDefault();
        const newName = event.target.elements.personName.value;
        event.target.elements.personName.value = '';
        this.props.addPerson(newName);
    },
    render() {
        const {items, people, subTotal, tax, tip} = this.props;
        return (
            <div className="col col-md-2" id="peopleList">
                {people.map((person) => {
                    return (<Person name={person.name}
                                    itemsBought={this.getItemsBoughtForPerson(person, items)}
                                    tipPercent={calculateTipPercent(subTotal, tax, tip)}
                                    taxPercent={calculateTaxPercent(subTotal, tax)}
                                    removePerson={this.props.removePerson}
                                    id={person.id}
                                    key={person.id}
                            />);
                })}
                <form onSubmit={this.addPerson}>
                    <input id="personName" type="text" placeholder="name" />
                    <button>+ person</button>
                </form>
            </div>
        );
    },
});

export default connect(
    function(state) {
        const {subTotal, tax, tip} = state.recipt;
        const {items} = state.item;
        const {people} = state.person;
        return {
            subTotal,
            tax,
            tip,
            items,
            people,
        };
    },
    function(dispatch) {
        return {
            addPerson: (name) => dispatch(actionCreators.addPerson(name)),
            removePerson: (id) => dispatch(actionCreators.removePerson(id)),
        };
    }
)(PeopleContainer);

module.exports = PeopleContainer;
