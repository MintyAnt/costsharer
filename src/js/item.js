
import React from 'react';

const Item = React.createClass({
    propTypes: {
        id: React.PropTypes.number.isRequired,
        value: React.PropTypes.any.isRequired,
        purchaserId: React.PropTypes.number.isRequired,
        purchaserName: React.PropTypes.string.isRequired,
        name: React.PropTypes.string.isRequired,
        people: React.PropTypes.array.isRequired,
        editItem: React.PropTypes.func.isRequired,
        removeItem: React.PropTypes.func.isRequired,
        duplicateItem: React.PropTypes.func.isRequired,
    },
    reConstructItem() {
        const {id, name, value, purchaserId, purchaserName} = this.props;
        return { id, name, value, purchaser: {purchaserId, purchaserName} };
    },
    handleChangeName(event) {
        this.props.editItem(this.props.id, {name: event.target.value});
    },
    handleChangeValue(event) {
        this.props.editItem(this.props.id, {value: event.target.valueAsNumber});
    },
    handleChangePurchaser(event) {
        const selectedPersonOption = event.target.selectedOptions[0];
        const personName = selectedPersonOption.innerText;
        const personId = parseInt(selectedPersonOption.value, 10);
        const item = {purchaser: { name: personName, id: personId}};
        this.props.editItem(this.props.id, item);
    },
    removeItem(event) {
        event.preventDefault();
        this.props.removeItem(this.props.id);
    },
    duplicateItem(event) {
        event.preventDefault();
        this.props.duplicateItem(this.props.id);
    },
    render() {
        return (
            <div className="row">
                <div className="col col-md-6" id="itemName">
                    <input type="text" className="form-control input-sm" defaultValue={this.props.name} placeholder="item name" onChange={this.handleChangeName} />
                </div>
                <div className="col col-md-6" id="itemValuePurchaser">
                    <div className="row" id="valuePurchaserBox">
                        <div className="col col-md-6" id="value">
                            <input type="number" step="0.01" min="0" className="form-control input-sm" defaultValue={this.props.value} placeholder="price" onChange={this.handleChangeValue} />
                        </div>
                        <div className="col col-md-6" id="purchaserBox">
                            <select className="form-control" id="peopleMenu" defaultValue={this.props.purchaserId} onChange={this.handleChangePurchaser}>
                                <option value="-1">select a name</option>
                                {this.props.people.map((person) => {
                                    return (<option key={person.id} value={person.id}>
                                                {person.name}
                                            </option>);
                                })}
                            </select>
                        </div>
                    </div>
                    <div className="row" id="outerPurchasers">
                        <div className="row" id="purchaserRow">
                            <div className="col col-md-2" id="type">type</div>
                            <div className="col col-md-4" id="value">value</div>
                            <div className="col col-md-4" id="purchaser">purchaser</div>
                        </div>
                        <div className="row" id="purchaserRow">
                            <div className="col col-md-2" id="type">type</div>
                            <div className="col col-md-4" id="value">value</div>
                            <div className="col col-md-4" id="purchaser">purchaser</div>
                        </div>
                    </div>
                </div>


                <form className="btn-group" onSubmit={this.removeItem}>
                    <button className="btn btn-custom-red">-</button>
                </form>
                <span className="input-group-btn" style={{width: '0px'}}></span>
                <form className="btn-group" onSubmit={this.duplicateItem}>
                    <button className="btn btn-default">+</button>
                </form>
            </div>
        );
    },
});

module.exports = Item;
