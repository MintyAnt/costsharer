
import _ from 'lodash';

let nextItemId = 0;

function getNextItemId() {
    const currentItemId = nextItemId;
    nextItemId++;
    return currentItemId;
}

export function calculateOwedForPerson(personTotal, taxPercent, tipPercent) {
    return (personTotal * (1 + tipPercent)) * (1 + taxPercent);
}

export function calculateTotalWithTax(subTotal, tax) {
    return (subTotal + tax) || 0;
}

export function calculateTotal(subTotal, tax, tip) {
    return (calculateTotalWithTax(subTotal, tax) + tip) || 0;
}

export function calculateTaxPercent(subTotal, tax) {
    return (tax / subTotal) || 0;
}

export function calculateTipPercent(subTotal, tax, tip) {
    return (tip / calculateTotalWithTax(subTotal, tax)) || 0;
}

export function generateItem(itemOverrides) {
    let id = -1;
    if (itemOverrides === undefined || id === undefined || id === -1) {
        id = getNextItemId();
    } else id = itemOverrides.id;

    const defaultItem = {
        id,
        name: '',
        value: 0,
        purchaser: {
            name: '',
            id: -1,
        },
    };
    return _.merge(defaultItem, itemOverrides);
}
