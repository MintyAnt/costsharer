
import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';

import Recipt from './recipt.js';
import { itemReducer as item } from './redux/itemReducer.js';
import { personReducer as person } from './redux/personReducer.js';
import { reciptReducer as recipt } from './redux/reciptReducer.js';

const intlData = {
    'locales': 'en-US',
    'formats': {
        'number': {
            'USD': {
                'style': 'currency',
                'currency': 'USD',
                'minimumFractionDigits': 2,
                'maximumFractionDigits': 2,
            },
        },
    },
};

const costSharerReducer = combineReducers({
    item,
    person,
    recipt,
});
const store = createStore(costSharerReducer);

ReactDOM.render(
    <Provider store={store}>
        <Recipt {...intlData} />
    </Provider>,
    document.getElementById('costSharer')
);
