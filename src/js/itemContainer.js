
import React from 'react';
import { connect } from 'react-redux';

import * as actionCreators from './redux/action-creators';
import Item from './item.js';

const ItemContainer = React.createClass({
    propTypes: {
        people: React.PropTypes.array.isRequired,
        items: React.PropTypes.array.isRequired,
        addItem: React.PropTypes.func.isRequired,
        editItem: React.PropTypes.func.isRequired,
        removeItem: React.PropTypes.func.isRequired,
        duplicateItem: React.PropTypes.func.isRequired,
    },
    addItem(event) {
        event.preventDefault();
        this.props.addItem();
    },
    render() {
        const {items, people} = this.props;
        return (
            <div className="col col-md-7" id="itemsList">
                {items.map((item) => {
                    return (<Item name={item.name}
                                    value={item.value}
                                    purchaserName={item.purchaser.name || ''}
                                    purchaserId={item.purchaser.id}
                                    people={people}
                                    editItem={this.props.editItem}
                                    removeItem={this.props.removeItem}
                                    duplicateItem={this.props.duplicateItem}
                                    id={item.id}
                                    key={item.id}
                            />);
                })}
                <form onSubmit={this.addItem}>
                    <button>+ item</button>
                </form>
            </div>
        );
    },
});

export default connect(
    function(state) {
        const {items} = state.item;
        const {people} = state.person;
        return {
            items,
            people,
        };
    },
    function(dispatch) {
        return {
            addItem: (name, value, purchaser) => dispatch(actionCreators.addItem(name, value, purchaser)),
            editItem: (id, editItem) => dispatch(actionCreators.editItem(id, editItem)),
            removeItem: (index) => dispatch(actionCreators.removeItem(index)),
            duplicateItem: (index) => dispatch(actionCreators.duplicateItem(index)),
        };
    }
)(ItemContainer);

module.exports = ItemContainer;
