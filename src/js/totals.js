
import React from 'react';
import { connect } from 'react-redux';

import * as actionCreators from './redux/action-creators';
import { calculateTotalWithTax, calculateTaxPercent, calculateTipPercent, calculateTotal } from './utils.js';


const Totals = React.createClass({
    propTypes: {
        subTotal: React.PropTypes.number.isRequired,
        tax: React.PropTypes.number.isRequired,
        tip: React.PropTypes.number.isRequired,
        whoPaidId: React.PropTypes.number.isRequired,
        subTotalFromItems: React.PropTypes.number.isRequired,
        totalFromItems: React.PropTypes.number.isRequired,
        reciptName: React.PropTypes.string.isRequired,
        people: React.PropTypes.array.isRequired,
        editSubTotal: React.PropTypes.func.isRequired,
        editTax: React.PropTypes.func.isRequired,
        editTip: React.PropTypes.func.isRequired,
        setWhoPaid: React.PropTypes.func.isRequired,
        setReciptName: React.PropTypes.func.isRequired,
    },
    moveExpected(event) {
        event.preventDefault();
        this.props.editSubTotal(this.props.subTotalFromItems);
    },
    handleChangeWhoPaid(event) {
        event.preventDefault();
        const selectedPersonOption = event.target.selectedOptions[0];
        this.props.setWhoPaid(parseInt(selectedPersonOption.value, 10));
    },
    handleSubTotalChange(event) {
        const newSubTotal = event.target.valueAsNumber;
        if (newSubTotal >= 0) {
            this.props.editSubTotal(newSubTotal);
        }
    },
    handleTaxChange(event) {
        const newTax = event.target.valueAsNumber;
        if (newTax >= 0) {
            this.props.editTax(newTax);
        }
    },
    handleTaxPercentChange(event) {
        const newTaxPercent = parseInt(event.target.value, 10);
        if (newTaxPercent >= 0) {
            const newTax = this.props.subTotal * (newTaxPercent / 100.0);
            this.props.editTax(newTax);
        }
    },
    handleTipChange(event) {
        const newTip = event.target.valueAsNumber;
        if (newTip >= 0) {
            this.props.editTip(newTip);
        }
    },
    handleTipPercentChange(event) {
        const newTipPercentage = parseInt(event.target.value, 10) || 0;
        if (newTipPercentage >= 0) {
            const {subTotal, tax, tip} = this.props;
            const newTip = calculateTotal(subTotal, tax, tip) * (newTipPercentage / 100.0);
            this.props.editTip(newTip);
        }
    },
    handleReciptNameChange(event) {
        this.props.setReciptName(event.target.value);
    },
    render() {
        const {subTotal, tax, tip, subTotalFromItems, totalFromItems} = this.props;
        const total = calculateTotal(subTotal, tax, tip);

        let expectedSubLabelClass = 'label label-success';
        if (subTotalFromItems !== subTotal) {
            expectedSubLabelClass = 'label label-danger';
        }

        let expectedTotalLabelClass = 'label label-success';
        if (totalFromItems !== total) {
            expectedTotalLabelClass = 'label label-danger';
        }

        return (
            <div className="col col-md-3" id="totals">
                <div>
                    Name this recipt
                    <input type="value" value={this.props.reciptName} onChange={this.handleReciptNameChange} />
                </div>
                <div>
                    Who paid?
                    <select className="form-control" id="peopleMenu" defaultValue={this.props.whoPaidId} onChange={this.handleChangeWhoPaid}>
                        <option value="-1">select a name</option>
                        {this.props.people.map((person) => {
                            return (<option key={person.id} value={person.id}>
                                        {person.name}
                                    </option>);
                        })}
                    </select>
                </div>
                <div>
                    <span>subTotal <input type="number" step="0.01" min="0" id="subTotal" value={subTotal} onChange={this.handleSubTotalChange} /></span>
                    <button onClick={this.moveExpected}>&lt;-</button>
                    <span className={expectedSubLabelClass}>expected {subTotalFromItems}</span>
                </div>
                <div>
                    <span>tax <input type="number" step="0.01" min="0" value={this.props.tax} onChange={this.handleTaxChange} /></span>
                    <span> % <input type="value" value={(calculateTaxPercent(subTotal, tax) * 100)} onChange={this.handleTaxPercentChange} /></span>
                </div>
                <div>total w/ tax {calculateTotalWithTax(subTotal, tax)}</div>
                <div>
                    <span>tip <input type="number" step="0.01" min="0" value={this.props.tip} onChange={this.handleTipChange} /></span>
                    <span> % <input type="value" value={calculateTipPercent(subTotal, tax, tip) * 100} onChange={this.handleTipPercentChange} /></span>
                </div>
                <div>
                    <span>total {total}</span>
                    <span className={expectedTotalLabelClass}>expected {totalFromItems}</span>
                </div>
                <div id="options">
                    <form onSubmit={this.generateJson}>
                        <button className="btn btn-success">save</button>
                    </form>
                    <span className="btn btn-success fileinput-button">
                        <span>load</span>
                        <input type="file" name="file" onChange={this.loadJson} />
                    </span>
                </div>
            </div>
        );
    },
});

export default connect(
    function(state) {
        return {
            reciptName: state.recipt.reciptName,
            whoPaidId: state.recipt.whoPaidId,
            subTotal: state.recipt.subTotal,
            tax: state.recipt.tax,
            tip: state.recipt.tip,
        };
    },
    function(dispatch) {
        return {
            editSubTotal: (newSubTotal) => dispatch(actionCreators.editSubTotal(newSubTotal)),
            editTax: (newTax) => dispatch(actionCreators.editTax(newTax)),
            editTip: (newTip) => dispatch(actionCreators.editTip(newTip)),
            setWhoPaid: (newId) => dispatch(actionCreators.setWhoPaid(newId)),
            setReciptName: (reciptName) => dispatch(actionCreators.setReciptName(reciptName)),
        };
    }
)(Totals);

module.exports = Totals;
