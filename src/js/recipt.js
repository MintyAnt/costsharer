
import _ from 'lodash';
import React from 'react';
import ReactIntl from 'react-intl';
import { connect } from 'react-redux';

import * as actionCreators from './redux/action-creators';
import ItemContainer from './itemContainer.js';
import PeopleContainer from './peopleContainer.js';
import Totals from './totals.js';
import { calculateOwedForPerson, calculateTaxPercent, calculateTipPercent } from './utils.js';


const IntlMixin = ReactIntl.IntlMixin;

const Recipt = React.createClass({
    propTypes: {
        tax: React.PropTypes.number.isRequired,
        tip: React.PropTypes.number.isRequired,
        subTotal: React.PropTypes.number.isRequired,
        whoPaidId: React.PropTypes.number.isRequired,
        reciptName: React.PropTypes.string.isRequired,
        items: React.PropTypes.array.isRequired,
        people: React.PropTypes.array.isRequired,
        addItem: React.PropTypes.func.isRequired,
        addPerson: React.PropTypes.func.isRequired,
        loadState: React.PropTypes.func.isRequired,
        removePerson: React.PropTypes.func.isRequired,
        editItem: React.PropTypes.func.isRequired,
        removeItem: React.PropTypes.func.isRequired,
        duplicateItem: React.PropTypes.func.isRequired,
    },
    mixins: [IntlMixin],
    calculateSubFromItems() {
        return _.sum(this.props.items, 'value');
    },
    calculateTotalFromItems() {
        const subTotalFromItems = this.calculateSubFromItems();
        const {subTotal, tax, tip} = this.props;
        const taxPercent = calculateTaxPercent(subTotal, tax);
        const tipPercent = calculateTipPercent(subTotal, tax, tip);
        return calculateOwedForPerson(subTotalFromItems, taxPercent, tipPercent);
    },
    addItem(event) {
        event.preventDefault();
        this.props.addItem();
    },
    generateJson(event) {
        event.preventDefault();

        const {reciptName, whoPaidId, subTotal, tip, tax, people, items} = this.props;
        const reciptJson = JSON.stringify({
            reciptName,
            whoPaidId,
            subTotal,
            tip,
            tax,
            people,
            items,
        });

        const blob = new Blob([reciptJson]);
        const link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = 'recipt.json';
        link.click();
    },
    loadJson(event) {
        event.preventDefault();
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.onload = (() => {
            return (e) => {
                const parsed = JSON.parse(e.target.result);
                this.props.loadState(parsed);
            };
        })(file);
        reader.readAsText(file);
    },
    render() {
        const {subTotal, tax, tip, items, people} = this.props;
        return (
            <div className="row">
                <Totals people={people}
                        subTotalFromItems={this.calculateSubFromItems()}
                        totalFromItems={this.calculateTotalFromItems()}
                />
                <ItemContainer />
                <PeopleContainer />
            </div>
        );
    },
});

export default connect(
    function(state) {
        const {reciptName, whoPaidId, subTotal, tax, tip} = state.recipt;
        const {items} = state.item;
        const {people} = state.person;
        return {
            reciptName,
            whoPaidId,
            subTotal,
            tax,
            tip,
            items,
            people,
        };
    },
    function(dispatch) {
        return {
            addItem: (name, value, purchaser) => dispatch(actionCreators.addItem(name, value, purchaser)),
            editItem: (id, editItem) => dispatch(actionCreators.editItem(id, editItem)),
            removeItem: (index) => dispatch(actionCreators.removeItem(index)),
            duplicateItem: (index) => dispatch(actionCreators.duplicateItem(index)),

            addPerson: (name) => dispatch(actionCreators.addPerson(name)),
            removePerson: (id) => dispatch(actionCreators.removePerson(id)),

            loadState: (newState) => dispatch(actionCreators.loadState(newState)),
        };
    }
)(Recipt);

module.exports = Recipt;
