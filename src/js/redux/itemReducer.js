
import _ from 'lodash';
import { generateItem } from '../utils.js';

const initialState = {
    items: [],
};

export function itemReducer(state = initialState, action) {
    switch (action.type) {
    case 'ADD_ITEM': {
        const nextItem = generateItem();
        return {
            ...state,
            items: state.items.concat(nextItem),
        };
    }
    case 'EDIT_ITEM': {
        const editIndex = _.findIndex(state.items, {id: action.id});
        const newItems = [...state.items];
        const updatedItem = _.merge({}, newItems[editIndex], action.itemEdits);
        if (editIndex !== -1) {
            newItems[editIndex] = updatedItem;
        } else {
            throw new Error('No item found of id ' + action.id);
        }
        return {
            ...state,
            items: newItems,
        };
    }
    case 'REMOVE_ITEM': {
        const newItems = state.items.filter((item) => item.id !== action.id);
        return {
            ...state,
            items: newItems,
        };
    }
    case 'DUPLICATE_ITEM': {
        const {items} = state;
        const chostenItemIndex = items.findIndex((item) => item.id === action.id);
        const newId = generateItem().id;
        const copied = {...items[chostenItemIndex], id: newId};
        const insertIndex = chostenItemIndex + 1;
        const newItems = items.slice(0, insertIndex).concat(copied).concat(items.slice(insertIndex, items.length));
        return {
            ...state,
            items: newItems,
        };
    }
    default:
        return state;
    }
}
