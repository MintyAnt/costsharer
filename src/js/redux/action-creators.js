
export function addItem(name, value, purchaser) {
    const newItem = {name: name || '', value: value || '', purchaser: purchaser || ''};
    return {
        type: 'ADD_ITEM',
        value: newItem,
    };
}

export function editItem(id, itemEdits) {
    return {
        type: 'EDIT_ITEM',
        id,
        itemEdits,
    };
}

export function removeItem(id) {
    return {
        type: 'REMOVE_ITEM',
        id: id,
    };
}

export function duplicateItem(id) {
    return {
        type: 'DUPLICATE_ITEM',
        id: id,
    };
}

export function addPerson(name) {
    return {
        type: 'ADD_PERSON',
        name: name,
    };
}

export function removePerson(id) {
    return {
        type: 'REMOVE_PERSON',
        id: id,
    };
}

export function editSubTotal(newSubTotal) {
    return { type: 'EDIT_SUBTOTAL', value: newSubTotal };
}

export function editTax(newTax) {
    return { type: 'EDIT_TAX', value: newTax };
}

export function editTip(newTip) {
    return { type: 'EDIT_TIP', value: newTip };
}

export function setWhoPaid(newId) {
    return { type: 'SET_WHOPAID', value: newId };
}

export function loadState(newState) {
    return { type: 'LOAD_STATE', value: newState };
}

export function setReciptName(reciptName) {
    return { type: 'SET_RECIPT_NAME', value: reciptName };
}
