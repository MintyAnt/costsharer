
const initialState = {
    reciptName: '',
    whoPaidId: -1,
    subTotal: 0,
    tax: 0,
    tip: 0,
};

export function reciptReducer(state = initialState, action) {
    switch (action.type) {
    case 'EDIT_SUBTOTAL': return { ...state, subTotal: action.value };
    case 'EDIT_TAX': return { ...state, tax: action.value };
    case 'EDIT_TIP': return { ...state, tip: action.value };
    case 'SET_WHOPAID': return { ...state, whoPaidId: action.value };
    case 'LOAD_STATE': return {...state, ...action.value};
    case 'SET_RECIPT_NAME': return {...state, reciptName: action.value};
    default:
        return state;
    }
}
