
let nextPersonId = 0;

function getNextPersonId() {
    const currentPersonId = nextPersonId;
    nextPersonId++;
    return currentPersonId;
}

const initialState = {
    people: [],
};

export function personReducer(state = initialState, action) {
    switch (action.type) {
    case 'ADD_PERSON':
        const nextPerson = {name: action.name, id: getNextPersonId()};
        return {
            ...state,
            people: state.people.concat(nextPerson),
        };
    case 'REMOVE_PERSON':
        const newPeople = state.people.filter((person) => person.id !== action.id);
        return {
            ...state,
            people: newPeople,
        };
    default:
        return state;
    }
}
