
import _ from 'lodash';
import React from 'react';
//import ReactIntl from 'react-intl';

import { calculateOwedForPerson } from './utils.js';


/*
const FormattedNumber = ReactIntl.FormattedNumber;

                <div>
                    <span>subTotal: <FormattedNumber value={this.calculateSubTotal()} format="USD" />
                    </span>
                </div>
                <div>
                    <span>total: <FormattedNumber value={this.calculateTotal(this.props.addedPercentage)} format="USD" />
                    </span>
                </div>
*/
const Person = React.createClass({
    propTypes: {
        id: React.PropTypes.number.isRequired,
        tipPercent: React.PropTypes.number.isRequired,
        taxPercent: React.PropTypes.number.isRequired,
        name: React.PropTypes.string.isRequired,
        itemsBought: React.PropTypes.array.isRequired,
        removePerson: React.PropTypes.func.isRequired,
    },
    calculateTotal() {
        return _.reduce(this.props.itemsBought, function(memo, item) {
            return memo + item.value;
        }, 0);
    },
    calculateOwed() {
        const tipPercent = this.props.tipPercent;
        const taxPercent = this.props.taxPercent;
        return calculateOwedForPerson(this.calculateTotal(), taxPercent, tipPercent);
    },
    removePerson(event) {
        event.preventDefault();
        this.props.removePerson(this.props.id);
    },
    render() {
        return (
            <div className="row" id="person">
                <div className="panel-heading">{this.props.name}</div>
                <div className="panel-body">
                    <div className="input-group">
                        <span className="input-group-addon" id="basic-addon1">Total</span>
                        <p className="form-control" aria-describedby="basic-addon1">{this.calculateTotal()}</p>
                    </div>
                    <div className="input-group">
                        <span className="input-group-addon" id="basic-addon1">Owed</span>
                        <p className="form-control" aria-describedby="basic-addon1">{this.calculateOwed()}</p>
                    </div>
                    <form className="btn-group" onSubmit={this.removePerson}>
                        <button className="btn btn-custom-red">-</button>
                    </form>
                </div>
            </div>
        );
    },
});

module.exports = Person;
