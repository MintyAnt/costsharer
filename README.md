# Setup

1. Install nodejs and npm
2. npm install
3. npm install -g gulp
4. gulp

# About the code

The entry point is in src/index.html

The gulp file will bundle everything into public/js/bundle.js, as well as watch for changes to re-bundle, as well as reload a browser page